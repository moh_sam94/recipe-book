export const environment = {
  production: true,
  apiKey: 'AIzaSyAnHS2fXG1OFO6gddydRa_PmlIdCKo6-0o',
  signinUrl: function () {
    return (
      'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' +
      this.apiKey
    );
  },
  signupUrl: function () {
    return (
      'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=' +
      this.apiKey
    );
  },

  dbUrl:
    'https://recipe-book-db-45604-default-rtdb.firebaseio.com/recipes.json',
};
