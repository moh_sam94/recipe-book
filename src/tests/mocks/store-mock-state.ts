export const initialState = [
  {
    shoppingList: {
      ingredients: [
        {
          name: 'apples',
          amount: 5,
        },
        {
          name: 'Banana',
          amount: 3,
        },
      ],
      editedIngredient: null,
      editedIngredientIndex: -1,
    },
    auth: {
      user: null,
      authError: null,
      loading: false,
    },
    recipes: {
      recipes: [],
    },
  },
];
